# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())
def download(): 
    if "video" in request.args[0] and not auth.has_membership("video"):
        return "You don't have video privileges, sorry"
    return response.download(request,db)
def call(): return service()
### end requires

import json
from subprocess import call

def index():
    constructs = db(db.t_construct.id>0).select(orderby=db.t_construct.f_order)
    levels = {}
    for c in constructs:
        levels[c.f_name] = db(db.t_level.f_construct==c).select()
    welcome_text = db(db.t_special_text.f_name=="welcome").select().first()
    if welcome_text is not None:
        welcome_text = welcome_text.f_description
    else:
        welcome_text = "No welcome text has been added!"
    return locals()

def constructs():
    constructs = db(db.t_construct.id>0).select(orderby=db.t_construct.f_order)
    levels = {}
    for c in constructs:
        levels[c.f_name] = db(db.t_level.f_construct==c).select()
    welcome_text = db(db.t_special_text.f_name=="construct").select().first()
    if welcome_text is not None:
        welcome_text = welcome_text.f_description
    else:
        welcome_text = "No welcome text has been added!"
    return locals()

def construct():
    construct = db(db.t_construct.f_url.like(request.args[0], case_sensitive=False)).select().first()
    levels = db(db.t_level.f_construct==construct.id).select()
    sublevels = {}
    lessons = db(db.t_lesson.id>0).select()
    relevant_construct = db(db.t_relevant_construct.id>0).select()
    videos = db(db.t_video.f_construct==construct.id).select()
    return locals()

def connections():
    construct = db(db.t_construct.f_url.like('connections', case_sensitive=False)).select().first()
    levels = db(db.t_level.f_construct==construct.id).select()
    sublevels = {}
    lessons = db(db.t_lesson.id>0).select()
    relevant_construct = db(db.t_relevant_construct.id>0).select()
    videos = db(db.t_video.f_construct==construct.id).select()
    return locals()

def level():
    construct = db(db.t_construct.f_url.like(request.args[0], case_sensitive=False)).select().first()
    level = db(db.t_level.f_construct==construct.id)(db.t_level.f_number==int(request.args[1])).select().first()
    lessons = set()
    sublevels = db(db.t_sublevel.f_level==level).select()
    examples = {}
    big_ideas = {}
    big_idea_map = db(db.t_connection.id>0).select()
    for sublevel in sublevels:
        examples[sublevel.id] = db(db.t_relevant_examples.f_sublevel==sublevel).select()
        big_ideas[sublevel.id] = db(db.t_relevant_big_idea.f_sublevel==sublevel).select()
        for lesson in db(db.t_relevant_sublevel.f_sublevel==sublevel).select():
            lessons.add(lesson.f_lesson)
    videos = db(db.t_video.f_level==level.id).select()
    relevant_construct = db(db.t_relevant_construct.id>0).select()
    return locals()

def sublevel():
    construct = db(db.t_construct.f_url.like(request.args[0], case_sensitive=False)).select().first()
    level = db(db.t_level.f_construct==construct.id)(db.t_level.f_number==int(request.args[1])).select().first()
    sublevel = db(db.t_sublevel.f_level==level)(db.t_sublevel.f_letter.like(request.args[2], case_sensitive=False)).select().first()
    big_ideas = db(db.t_relevant_big_idea.f_sublevel==sublevel).select()
    big_idea_map = db(db.t_connection.id>0).select()
    examples = db(db.t_relevant_examples.f_sublevel==sublevel).select()
    lessons = set()
    for lesson in db(db.t_relevant_sublevel.f_sublevel==sublevel).select():
        lessons.add(lesson.f_lesson)
    videos = db(db.t_video.f_sublevel==sublevel.id).select()
    relevant_construct = db(db.t_relevant_construct.id>0).select()
    return locals()

def bigidea():
    big_idea = db(db.t_big_idea.id==int(request.args[0])).select().first()
    big_idea_map = db(db.t_connection.id>0).select()
    sublevels = db(db.t_relevant_big_idea.f_big_idea==big_idea).select()
    examples = {}
    for sublevel in sublevels:
        examples[sublevel.f_sublevel.id] = db(db.t_relevant_examples.f_sublevel==sublevel.f_sublevel).select()
    lessons = db(db.t_relevant_big_idea_2.f_big_idea==big_idea).select()
    return locals()

def lesson():
    lesson = db(db.t_lesson.id==int(request.args[0])).select().first()
    big_ideas = db(db.t_relevant_big_idea_2.f_lesson==lesson).select()
    big_idea_map = db(db.t_connection.id>0).select()
    sublevels = db(db.t_relevant_sublevel.f_lesson==lesson).select()
    constructs = db(db.t_relevant_construct.f_lesson==lesson).select()
    teachers_corner = db(db.t_teachers_corner.f_lesson==lesson)(db.t_teachers_corner.is_active==True)(db.t_teachers_corner.f_is_approved==True).select()
    additional_materials = teachers_corner.find(lambda row: row.f_author_override=="material")
    teachers_corner = teachers_corner.find(lambda row: row.f_author_override != "material")
    sublevels = sublevels.sort(lambda row: row.f_sublevel.f_level.f_construct.f_order)
    constructs = constructs.sort(lambda row: row.f_construct.f_order)
    videos = db(db.t_video.f_lesson==lesson.id).select()
    videos_tc = {}
    for tc in teachers_corner:
        videos_tc[tc.id] = db(db.t_video.f_teachers_corner == tc.id).select()
    return locals()

def lessons():
    if len(request.args) > 0:
        constructs = db(db.t_construct.f_url.like(request.args[0], case_sensitive=False)).select()
    else:
        constructs = db(db.t_construct.id>0).select(orderby=db.t_construct.f_order)
    lessons = db(db.t_lesson.id>0).select()
    relevant_construct = db(db.t_relevant_construct.id>0).select()
    welcome_text = db(db.t_special_text.f_name=="lesson").select().first()
    if welcome_text is not None:
        welcome_text = welcome_text.f_description
    else:
        welcome_text = "No welcome text has been added!"
    return locals()

@auth.requires_login()
def lessonmap():
    lesson = int(request.args[0])
    if request.vars.nodes:
        nodes = json.dumps(json.loads(request.vars.nodes))
        db(db.t_lesson.id==lesson).update(f_nodes = nodes)
    if request.vars.edges:
        edges = json.dumps(json.loads(request.vars.edges))
        db(db.t_lesson.id==lesson).update(f_edges = edges)
    if request.vars.options:
        options = json.dumps(json.loads(request.vars.options))
        db(db.t_lesson.id==lesson).update(f_options = options)
    return 'Hello'

def bigideas():
    big_ideas = db(db.t_big_idea.id>0).select(orderby=db.t_big_idea.f_description)
    return locals()

@auth.requires_login()
def submit_tc():
    form = SQLFORM(db.t_teachers_corner)
    if len(request.args) > 0:
        form.vars.f_lesson = request.args[0]
    else:
        redirect(URL('index'))
    if form.process().accepted:
        response.flash = 'form accepted'
    elif form.errors:
        response.flash = 'form has errors'
    else:
        response.flash = 'please fill out the form'
    return dict(form=form)

@auth.requires_login()
def edit_tc():
    if auth.has_membership("admin"):
        tc = db(db.t_teachers_corner.id==int(request.args[0])).select().first()
    else:
        tc = db(db.t_teachers_corner.id==int(request.args[0]))(db.t_teachers_corner.f_author==auth.user.id).select().first()
    form = SQLFORM(db.t_teachers_corner,tc)
    if form.process().accepted:
        response.flash = 'form accepted'
    elif form.errors:
        response.flash = 'form has errors'
    else:
        response.flash = 'please fill out the form'
    return dict(form=form)

@auth.requires_login()
def tc():
    if request.vars.delete:
        db(db.t_teachers_corner.f_author == auth.user)(db.t_teachers_corner.id==int(request.vars.delete)).update(is_active=False)
    teachers_corner = db(db.t_teachers_corner.f_author == auth.user)(db.t_teachers_corner.is_active==True).select()
    return locals()

@auth.requires_membership("admin")
def tc_approve():
    if request.vars.approve:
        db(db.t_teachers_corner.id==int(request.vars.approve)).update(f_is_approved=True)
    if request.vars.ignore:
        db(db.t_teachers_corner.id==int(request.vars.ignore)).update(is_active=False)
    teachers_corner = db(db.t_teachers_corner.f_is_approved == False).select()
    return locals()

@auth.requires_membership("admin")
def addlesson():
    form = SQLFORM(db.t_lesson,fields=['f_name','f_subtitle','f_overview','f_complete_lesson','f_far'])
    if form.process().accepted:
        response.flash = 'form accepted'
        redirect(URL("thumbnail",args=form.vars.id))
    elif form.errors:
        response.flash = 'form has errors'
    return dict(form=form)

@auth.requires_membership("admin")
def editlesson():
    lesson = db(db.t_lesson.id==int(request.args[0])).select().first()
    form = SQLFORM(db.t_lesson,request.args[0],fields=['f_name','f_subtitle','f_overview','f_complete_lesson','f_far'])
    if form.process().accepted:
        response.flash = 'form accepted'
        redirect(URL("thumbnail",args=request.args[0]))
    elif form.errors:
        response.flash = 'form has errors'
    form2 = SQLFORM.factory(
        Field('lesson_to_copy', requires=IS_IN_DB(db, 't_lesson.id', '%(f_name)s')),
        table_name='copy_lesson_map')
    if form2.process().accepted:
        lesson_to_copy = db.t_lesson[int(form2.vars.lesson_to_copy)]
        db(db.t_lesson.id==int(request.args[0])).update(f_nodes=lesson_to_copy.f_nodes,f_edges=lesson_to_copy.f_edges)
        redirect(URL("lesson",args=request.args[0]))
    elif form2.errors:
        response.flash = 'form has errors'
    sublevels = db(db.t_relevant_sublevel.f_lesson==int(request.args[0])).select()
    form3 = SQLFORM.factory(
        Field('sublevels', 
            label="Choose sublevels here. Copy and paste the degree symbol if needed: °",
            requires=IS_IN_DB(db, 't_sublevel.id', '%(f_name)s %(f_description)s', multiple=True), default=[s.f_sublevel.id for s in sublevels]),
        table_name='set_sublevels')
    if form3.process().accepted:
        db(db.t_relevant_sublevel.f_lesson==int(request.args[0])).delete()
        for sublevel in form3.vars.sublevels:
            db.t_relevant_sublevel.insert(f_lesson=int(request.args[0]),f_sublevel=int(sublevel))
        redirect(URL("lesson",args=request.args[0]))
    elif form3.errors:
        response.flash = 'form has errors'
    constructs = db(db.t_relevant_construct.f_lesson==int(request.args[0])).select()
    form4 = SQLFORM.factory(
        Field('constructs', 
            label="Choose constructs here:",
            requires=IS_IN_DB(db, 't_construct.id', '%(f_name)s (%(f_url)s)', multiple=True), default=[c.f_construct.id for c in constructs]),
        table_name='set_constructs')
    if form4.process().accepted:
        db(db.t_relevant_construct.f_lesson==int(request.args[0])).delete()
        for construct in form4.vars.constructs:
            db.t_relevant_construct.insert(f_lesson=int(request.args[0]),f_construct=int(construct))
        redirect(URL("lesson",args=request.args[0]))
    elif form3.errors:
        response.flash = 'form has errors'
    return dict(form=form, form2=form2, form3=form3, form4=form4, lesson=lesson)

@auth.requires_membership("admin")
def addconstruct():
    form = SQLFORM(db.t_construct)
    if form.process().accepted:
        response.flash = 'form accepted'
        redirect(URL("construct",args=form.vars.f_url))
    elif form.errors:
        response.flash = 'form has errors'
    return dict(form=form, construct=construct)

@auth.requires_membership("admin")
def editconstruct():
    construct = db(db.t_construct.f_name.ilike(request.args[0])).select().first()
    form = SQLFORM(db.t_construct,construct.id,deletable=True)
    if form.process().accepted:
        response.flash = 'form accepted'
        redirect(URL("construct",args=construct.f_url))
    elif form.errors:
        response.flash = 'form has errors'
    return dict(form=form, construct=construct)

@auth.requires_membership("admin")
def editspecialtext():
    special_text = db(db.t_special_text.f_name.ilike(request.args[0])).select().first()
    if special_text:
        form = SQLFORM(db.t_special_text,special_text.id)
    else:
        form = SQLFORM(db.t_special_text)
        form.vars.f_name = request.args[0]
    if form.process().accepted:
        response.flash = 'form accepted'
        redirect(form.vars.f_url)
    elif form.errors:
        response.flash = 'form has errors'
    return dict(form=form, special_text=special_text)

@auth.requires_membership("admin")
def addlevel():
    construct = db(db.t_construct.f_name.like(request.args[0], case_sensitive=False)).select().first()
    form = SQLFORM(db.t_level)
    if form.process().accepted:
        response.flash = 'form accepted'
        redirect(URL("construct",args=[construct.f_url]))
    elif form.errors:
        response.flash = 'form has errors'
    return dict(form=form, construct=construct, level=level)

@auth.requires_membership("admin")
def editlevel():
    construct = db(db.t_construct.f_name.like(request.args[0], case_sensitive=False)).select().first()
    level = db(db.t_level.f_construct==construct.id)(db.t_level.f_number==int(request.args[1])).select().first()
    form = SQLFORM(db.t_level,level.id,deletable=True)
    if form.process().accepted:
        response.flash = 'form accepted'
        redirect(URL("level",args=[construct.f_url,level.f_number]))
    elif form.errors:
        response.flash = 'form has errors'
    return dict(form=form, level=level)

### this doesn't work yet ###
@auth.requires_membership("admin")
def addsublevel():
    construct = db(db.t_construct.f_url.like(request.args[0], case_sensitive=False)).select().first()
    level = db(db.t_level.f_construct==construct.id)(db.t_level.f_number==int(request.args[1])).select().first()
    form = SQLFORM(db.t_sublevel)
    if form.process().accepted:
        response.flash = 'form accepted'
        redirect(URL("level",args=[construct.f_url,level.f_number]))
    elif form.errors:
        response.flash = 'form has errors'
    return dict(form=form, construct=construct, level=level)

@auth.requires_membership("admin")
def editsublevel():
    construct = db(db.t_construct.f_url.like(request.args[0], case_sensitive=False)).select().first()
    level = db(db.t_level.f_construct==construct.id)(db.t_level.f_number==int(request.args[1])).select().first()
    sublevel = db(db.t_sublevel.f_level==level)(db.t_sublevel.f_letter.like(request.args[2], case_sensitive=False)).select().first()
    examples = db(db.t_relevant_examples.f_sublevel==sublevel).select()
    form = SQLFORM(db.t_sublevel,sublevel.id,deletable=True)
    if form.process().accepted:
        response.flash = 'form accepted'
        redirect(URL("sublevel",args=[construct.f_url,level.f_number,sublevel.f_letter]))
    elif form.errors:
        response.flash = 'form has errors'
    return dict(form=form, sublevel=sublevel, examples=examples)

@auth.requires_membership("admin")
def addexample():
    sublevel = db(db.t_sublevel.id==int(request.args[0])).select().first()
    form = SQLFORM(db.t_example)
    if form.process().accepted:
        db.t_relevant_examples.insert(f_sublevel=sublevel,f_example=form.vars.id)
        response.flash = 'form accepted'
        redirect(URL("editsublevel",args=[sublevel.f_level.f_construct.f_url,
            sublevel.f_level.f_number,sublevel.f_letter.lower()]))
    elif form.errors:
        response.flash = 'form has errors'
    return dict(form=form, sublevel=sublevel)

@auth.requires_membership("admin")
def editexample():
    sublevel = db(db.t_sublevel.id==int(request.args[0])).select().first()
    example = db(db.t_example.id==int(request.args[1])).select().first()
    form = SQLFORM(db.t_example,example.id,deletable=True)
    if form.process().accepted:
        response.flash = 'form accepted'
        redirect(URL("editsublevel",args=[sublevel.f_level.f_construct.f_url,
            sublevel.f_level.f_number,sublevel.f_letter.lower()]))
    elif form.errors:
        response.flash = 'form has errors'
    return dict(form=form, example=example)

@auth.requires_membership("admin")
def addvideo():
    form = SQLFORM(db.t_video)
    if form.process().accepted:
        response.flash = 'form accepted'
    elif form.errors:
        response.flash = 'form has errors'
    return dict(form=form, sublevel=sublevel)

def error():
    return dict()

@auth.requires_membership('admin')
def userlist():
    users = db(db.auth_user.id>0).select()
    return dict(users=users)

@auth.requires_membership('admin')
def makeadmin():
    auth.add_membership('admin',request.args[0])
    redirect(URL("userlist"))

def makefirstuseradmin():
    auth.add_group('admin')
    auth.add_group('video')
    auth.add_membership('admin',1)
    redirect(URL("userlist"))

@auth.requires_membership('admin')
def removeadmin():
    auth.del_membership('admin',request.args[0])
    redirect(URL("userlist"))
    
@auth.requires_membership('admin')
def makevideo():
    auth.add_membership('video',request.args[0])
    redirect(URL("userlist"))

@auth.requires_membership('admin')
def removevideo():
    auth.del_membership('video',request.args[0])
    redirect(URL("userlist"))

@auth.requires_membership('admin')
def thumbnail():
    lesson = db(db.t_lesson.id==int(request.args[0])).select().first()
    if request.vars.generate:
        call(['convert -density 300 %s/uploads/%s[0] %s/static/thumbnails/%s.png' % 
            (request.folder,lesson.f_complete_lesson,request.folder,lesson.f_complete_lesson)],shell=True)
        redirect(URL("editlesson",args=request.args[0]))
    return locals()