from gluon.storage import Storage
settings = Storage()

settings.migrate = True
settings.title = 'KidViz'
settings.subtitle = ''
settings.author = 'Dan Burger'
settings.author_email = 'dan.burger@vanderbilt.edu'
settings.keywords = 'education learning math science peabody vanderbilt length area volume angle'
settings.description = 'KidViz aims to provide math and science teachers in grades 1-5 with classroom activities and assessments centered around four types of spatial measurements: lengths, areas, volumes, and angles. It is sponsored by Corey Brady and Rich Lehrer, both professors at the Department of Teaching and Learning at Vanderbilt’s Peabody College.'
settings.layout_theme = 'Default'
settings.database_uri = 'sqlite://storage.sqlite'
settings.security_key = '2c2e568f-e9c5-4b08-8c3b-35ea6c305800'
settings.email_server = 'localhost'
settings.email_sender = 'you@example.com'
settings.email_login = ''
settings.login_method = 'local'
settings.login_config = ''
settings.plugins = []
