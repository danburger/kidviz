### we prepend t_ to tablenames and f_ to fieldnames for disambiguity

# Special text used throughout the site
########################################
db.define_table('t_special_text',
    Field('f_name', type='string',
          label=T('Name')),
    Field('f_description', type='text',
          label=T('Description')),
    Field('f_url', type='string',
          label=T('URL where it appears')),
    auth.signature,
    format='%(f_name)s',
    migrate=settings.migrate)

db.define_table('t_special_text_archive',db.t_special_text,Field('current_record','reference t_special_text',readable=False,writable=False))

# Examples for each sublevel, to be linked with t_relevant_examples
########################################
db.define_table('t_example',
    Field('f_description', type='text',
          label=T('Description')),
    auth.signature,
    format='%(f_description)s',
    migrate=settings.migrate)

db.define_table('t_example_archive',db.t_example,Field('current_record','reference t_example',readable=False,writable=False))

# Construct (length, area, volume, angle)
########################################
db.define_table('t_construct',
    Field('f_name', type='string',
          label=T('Name (e.g. Length)')),
    Field('f_long_name', type='string',
          label=T('Long Name (e.g. Theory of Measurement - Length)')),
    Field('f_icon', type='string',
          label=T('Icon')),
    Field('f_short_name', type='string',
          label=T('Short Name (e.g. ToML)')),
    Field('f_short_desc', type='text',
          label=T('Short Description')),
    Field('f_description', type='text',
          label=T('Description')),
    Field('f_url', type='string',
          label=T('URL')),
    Field('f_order', type='integer',
          label=T('Order')),
    auth.signature,
    format='%(f_name)s',
    migrate=settings.migrate)

db.define_table('t_construct_archive',db.t_construct,Field('current_record','reference t_construct',readable=False,writable=False))

# Images for each example
########################################
db.define_table('t_example_img',
    Field('f_name', type='string',
          label=T('Name')),
    Field('f_file', type='upload',
          label=T('File')),
    auth.signature,
    format='%(f_example)s',
    migrate=settings.migrate)

db.define_table('t_example_img_archive',db.t_example_img,Field('current_record','reference t_example_img',readable=False,writable=False))

# Level (for instance, ToML1)
########################################
db.define_table('t_level',
    Field('f_construct', type='reference t_construct',
          label=T('Construct')),
    Field('f_number', type='integer',
          label=T('Number')),
    Field('f_name', type='string',
          label=T('Name')),
    Field('f_short_desc', type='string',
          label=T('Short Description')),
    Field('f_long_desc', type='text',
          label=T('Long Description')),
    auth.signature,
    format='%(f_name)s',
    migrate=settings.migrate)

db.define_table('t_level_archive',db.t_level,Field('current_record','reference t_level',readable=False,writable=False))

# Lesson (content is no longer used)
########################################
db.define_table('t_lesson',
    Field('f_name', type='string',
          label=T('Name')),
    Field('f_subtitle', type='string',
          label=T('Subtitle')),
    Field('f_slug', type='string',
          label=T('Slug')),
    Field('f_pages', type='integer',
          label=T('Number of Pages')),
    Field('f_content', type='text', requires=IS_LENGTH(200000),
          label=T('Archived Content')),
    Field('f_nodes', type='text', requires=IS_LENGTH(200000),
          label=T('Nodes')),
    Field('f_edges', type='text', requires=IS_LENGTH(200000),
          label=T('Edges')),
    Field('f_options', type='text', requires=IS_LENGTH(200000),
          label=T('Options')),
    Field('f_first_page', type='upload',
          label=T('First Page Image')),
    Field('f_complete_lesson', type='upload',
          label=T('Complete Lesson')),
    Field('f_far', type='upload',
          label=T('Formative Assessment Record')),
    Field('f_overview', type='text',
          label=T('Unit Overview. Use [[1/2]] to render fractions. For special symbols use <x>, <deg>, <2>, <3> (superscripts), <-1>, <-2>, <-3> (subscripts)')),
    auth.signature,
    format='%(f_name)s',
    migrate=settings.migrate)

db.define_table('t_lesson_archive',db.t_lesson,Field('current_record','reference t_lesson',readable=False,writable=False))

# Comments for each lesson - "Teacher's Corner"
########################################
db.define_table('t_comments',
    Field('f_lesson', type='reference t_lesson',
          label=T('Lesson')),
    Field('f_user', type='reference auth_user',
          label=T('User')),
    Field('f_content', type='text',
          label=T('Content')),
    Field('f_timestamp', type='datetime',
          label=T('Timestamp')),
    auth.signature,
    format='%(f_lesson)s',
    migrate=settings.migrate)

db.define_table('t_comments_archive',db.t_comments,Field('current_record','reference t_comments',readable=False,writable=False))

# Images used in level description
########################################
db.define_table('t_level_img',
    Field('f_name', type='string',
          label=T('Name')),
    Field('f_file', type='upload',
          label=T('File')),
    auth.signature,
    format='%(f_name)s',
    migrate=settings.migrate)

db.define_table('t_level_img_archive',db.t_level_img,Field('current_record','reference t_level_img',readable=False,writable=False))

# Sublevel (for instance, ToML1A)
########################################
db.define_table('t_sublevel',
    Field('f_level', type='reference t_level',
          label=T('Level')),
    Field('f_letter', type='string',
          label=T('Letter')),
    Field('f_name', type='string',
          label=T('Name')),
    Field('f_description', type='string',
          label=T('Description')),
    auth.signature,
    format='%(f_name)s',
    migrate=settings.migrate)

db.define_table('t_sublevel_archive',db.t_sublevel,Field('current_record','reference t_sublevel',readable=False,writable=False))

# Connect sublevel to lesson
########################################
db.define_table('t_relevant_sublevel',
    Field('f_lesson', type='reference t_lesson',
          label=T('Lesson')),
    Field('f_sublevel', type='reference t_sublevel',
          label=T('Sublevel')),
    Field('f_description', type="string",
          label=T('Description')),
    auth.signature,
    format='%(f_lesson)s',
    migrate=settings.migrate)

db.define_table('t_relevant_sublevel_archive',db.t_relevant_sublevel,Field('current_record','reference t_relevant_sublevel',readable=False,writable=False))

# Connect construct to lesson
########################################
db.define_table('t_relevant_construct',
    Field('f_lesson', type='reference t_lesson',
          label=T('Lesson')),
    Field('f_construct', type='reference t_construct',
          label=T('Construct')),
    auth.signature,
    format='%(f_lesson)s',
    migrate=settings.migrate)

db.define_table('t_relevant_construct_archive',db.t_relevant_construct,Field('current_record','reference t_relevant_construct',readable=False,writable=False))


# Connect sublevel to example
########################################
db.define_table('t_relevant_examples',
    Field('f_sublevel', type='reference t_sublevel',
          label=T('Sublevel')),
    Field('f_example', type='reference t_example',
          label=T('Example')),
    auth.signature,
    format='%(f_sublevel)s',
    migrate=settings.migrate)

db.define_table('t_relevant_examples_archive',db.t_relevant_examples,Field('current_record','reference t_relevant_examples',readable=False,writable=False))

# Big Idea
########################################
db.define_table('t_big_idea',
    Field('f_description', type='string',
          label=T('Description')),
    Field('f_comments', type='string',
          label=T('Comments')),
    auth.signature,
    format='%(f_description)s',
    migrate=settings.migrate)

db.define_table('t_big_idea_archive',db.t_big_idea,Field('current_record','reference t_big_idea',readable=False,writable=False))

# Connect big idea to lesson
########################################
db.define_table('t_relevant_big_idea_2',
    Field('f_lesson', type='reference t_lesson',
          label=T('Lesson')),
    Field('f_big_idea', type='reference t_big_idea',
          label=T('Big Idea')),
    auth.signature,
    format='%(f_lesson)s',
    migrate=settings.migrate)

db.define_table('t_relevant_big_idea_2_archive',db.t_relevant_big_idea_2,Field('current_record','reference t_relevant_big_idea_2',readable=False,writable=False))

# Connect big idea to sublevel
########################################
db.define_table('t_relevant_big_idea',
    Field('f_sublevel', type='reference t_sublevel',
          label=T('Lesson')),
    Field('f_big_idea', type='reference t_big_idea',
          label=T('Big Idea')),
    auth.signature,
    format='%(f_lesson)s',
    migrate=settings.migrate)

db.define_table('t_relevant_big_idea_archive',db.t_relevant_big_idea,Field('current_record','reference t_relevant_big_idea',readable=False,writable=False))

# Connect two big ideas
########################################
db.define_table('t_connection',
    Field('f_from', type='reference t_big_idea',
          label=T('From')),
    Field('f_to', type='reference t_big_idea',
          label=T('To')),
    auth.signature,
    format='%(f_from)s',
    migrate=settings.migrate)

db.define_table('t_connection_archive',db.t_connection,Field('current_record','reference t_connection',readable=False,writable=False))

# Teacher's Corner
########################################
db.define_table('t_teachers_corner',
    Field('f_lesson', type='reference t_lesson', readable=False, writable=True,
          label=T('Lesson')),
    Field('f_author', type='reference auth_user', readable=False, writable=False,
          default=auth.user,
          label=T('Lesson')),
    Field('f_name', type='string', required=True,
          label=T('What is this entry called?')),
    Field('f_author_override', type='string',
          label=T('Author (leave blank if you wrote it yourself)')),
    Field('f_author_override_email', type='string',
          label=T('Author Email (leave blank if you wrote it yourself)')),
    Field('f_content', type='text',
          label=T('Brief summary of entry')),
    Field('f_file', type='upload', required=True,
          label=T('File')),
    Field('f_is_approved', type='boolean', readable=False, writable=False, default=False,
          label=T('Is Approved')),
    auth.signature,
    format='%(f_name)s',
    migrate=settings.migrate)


db.define_table('t_teachers_corner_archive',db.t_connection,Field('current_record','reference t_teachers_corner',readable=False,writable=False))

# Videos
########################################
db.define_table('t_video',
    Field('f_name', type='string',
          label=T('Name')),
    Field('f_description', type='text',
          label=T('Description')),
    Field('f_file', type='upload',
          label=T('File')),
    Field('f_youtube_url', type='string',
          label=T('YouTube URL')),
    Field('f_construct', type='reference t_construct',
          label=T('Construct')),
    Field('f_level', type='reference t_level',
          label=T('Level')),
    Field('f_sublevel', type='reference t_sublevel',
          label=T('Sublevel')),
    Field('f_lesson', type='reference t_lesson',
          label=T('Lesson')),
    Field('f_teachers_corner', type='reference t_teachers_corner',
          label=T('Teacher\'s Corner')),
    auth.signature,
    format='%(f_description)s',
    migrate=settings.migrate)

db.define_table('t_video_archive',db.t_video,Field('current_record','reference t_video',readable=False,writable=False))