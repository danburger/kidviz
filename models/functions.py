#Converts formatting to HTML, adds images
def FORMAT(content):
    content = content.replace("[[x]]","×")
    content = content.replace("[[deg]]","°")
    content = content.replace("[[","<fraction>")
    content = content.replace("]]","</fraction>")
    content = content.replace("<u>","𝑢")
    content = content.replace("<unit>","<em>unit</em>")
    content = content.replace("<in>","<em>in</em>")
    content = content.replace("<inch>","<em>inch</em>")
    content = content.replace("<inches>","<em>inches</em>")
    content = content.replace("<ft>","<em>ft</em>")
    content = content.replace("<cm>","<em>cm</em>")
    content = content.replace("<m>","<em>m</em>")
    content = content.replace("<n>","<em>n</em>")
    content = content.replace("<x>","×")
    content = content.replace("<A>","<b>A</b>")
    content = content.replace("<B>","<b>B</b>")
    content = content.replace("<2>","<sup>2</sup>")
    content = content.replace("<3>","<sup>3</sup>")
    content = content.replace("<-1>","<sub>1</sub>")
    content = content.replace("<-2>","<sub>2</sub>")
    content = content.replace("<-3>","<sub>3</sub>")
    content = content.replace("<deg>","°")
    content = content.replace('“','"')
    content = content.replace('”','"')
    content = content.replace('\n\r\n','<br/>')
    if "level-img" in content:
        for image in db(db.t_level_img.id>0).select():
            content = content.replace("<level-img-%s>" % image.f_name,
                "<br><img title='figure' alt='' src='/%s/default/download/%s'>" % (request.application,image.f_file))
    if "example-img" in content:
        for image in db(db.t_example_img.id>0).select():
            content = content.replace("<example-img-%s>" % image.f_name,
                "<img title='figure' alt='' src='/%s/default/download/%s'>" % (request.application,image.f_file))
    if "<more>" in content:
        content = content.replace("<more>","<btn class='btn btn-lg btn-fill btn-info' onclick='$(\"#more_text\").toggle();$(\"#more_table\").toggle()'>read more</btn><span id='more_text' style='display:none'>")
        content += "</span>"
    return XML(content, sanitize=True, 
        permitted_tags=['span','b','em','img','p','sup','sub','div','ul','li','h1','h2','h3',
            'h4','h5','h6','ol','br','strong','blockquote','table','tr','td','btn','span','fraction'], 
        allowed_attributes={'span':['class'],'table':['id','class','style'],'td':['colspan'],'img':['src','alt','title'],'btn':['class','onclick'],'span':['id','style']})

#for simple text
def SIMPLEFORMAT(content):
    content = content.replace("[[x]]","x")
    content = content.replace("[[deg]]","°")
    content = content.replace("[[","")
    content = content.replace("]]","")
    content = content.replace("<u>","u")
    content = content.replace("<unit>","unit")
    content = content.replace("<in>","in")
    content = content.replace("<inch>","inch")
    content = content.replace("<inches>","inches")
    content = content.replace("<ft>","ft")
    content = content.replace("<cm>","cm")
    content = content.replace("<m>","m")
    content = content.replace("<n>","n")
    content = content.replace("<x>","x")
    content = content.replace("<A>","A")
    content = content.replace("<B>","B")
    content = content.replace("<2>","^2")
    content = content.replace("<3>","^3")
    content = content.replace("<-1>","_1")
    content = content.replace("<-2>","_2")
    content = content.replace("<-3>","_3")
    content = content.replace("<deg>","°")
    content = content.replace('“','"')
    content = content.replace('”','"')
    content = content.replace("&","and")
    content = content.replace("<","less than")
    content = content.replace(">","greater than")
    return content

def get_first_char(s):
    if s[0] in "()0123456789":
        return "#"
    if s[0] == "[":
        return "Fraction"
    return s[0]