response.title = settings.title
response.subtitle = settings.subtitle
response.meta.author = '%(author)s <%(author_email)s>' % settings
response.meta.keywords = settings.keywords
response.meta.description = settings.description
constructs = db(db.t_construct.id>0).select(orderby=db.t_construct.f_order)
if configuration.get('app.list_all_lessons_left'):
    lessons = db(db.t_lesson.id>0).select()
icons = {}
starred_menu_items = ['Constructs','Lessons']
response.menu = [
    ("Constructs",URL('default','constructs')==URL(),URL('default','constructs'))
]

for c in constructs:
    if c.f_url not in ['connections']:
        response.menu.append((T(c.f_name),URL('default','construct',args=[c.f_url])==URL(),URL('default','construct',args=[c.f_url]),[]),)
    #icons[c.f_name] = c.f_icon
response.menu.append(("",False,"#",[]))
response.menu.append(("Lessons",URL('default','lessons')==URL(),URL('default','lessons'),[]))
if configuration.get('app.list_all_lessons_left'):
    for l in lessons:
        response.menu.append((T("("+l.f_name.replace("Unit ","")+") "+l.f_subtitle),URL('default','lesson',args=[l.id])==URL(),URL('default','lesson',args=[l.id]),[]),)
else:
    for c in constructs:
        if c.f_name == "Connections":
            pass#response.menu.append((T(c.f_name),URL('default','connections')==URL(),URL('default','connections'),[]),)
        else:
            response.menu.append((T(c.f_name),URL('default','lessons',args=[c.f_url])==URL(),URL('default','lessons',args=[c.f_url]),[]),)
            #icons[c.f_name] = c.f_icon
pass
response.menu.append(("",False,"#",[]))


#response.menu.append(("Big Ideas",URL('default','bigideas')==URL(),URL('default','bigideas'),[]))

if auth.user_id:
    response.menu.append(("Your TC Entries",URL('default','tc')==URL(),URL('default','tc'),[]))

if auth.has_membership('admin'):
    response.menu.append(("User List",URL('default','userlist')==URL(),URL('default','userlist'),[]))
    count_tc = db(db.t_teachers_corner.f_is_approved==False)(db.t_teachers_corner.is_active==True).count()
    response.menu.append(("Approve TC Entries (%s)" % count_tc,URL('default','tc_approve')==URL(),URL('default','tc_approve'),[]))
else:
    count_tc = 0